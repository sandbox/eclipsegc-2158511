<?php
/**
 * @file
 * Contains DisplayListController.php.
 */

namespace Drupal\page_manager;

use Drupal\Core\Config\Entity\ConfigEntityListController;

class DisplayListController extends ConfigEntityListController {

  public function render() {
    $entities = $this->load();
    drupal_set_message('<pre>' . print_r($entities, TRUE) . '</pre>');
    return array();
  }

} 