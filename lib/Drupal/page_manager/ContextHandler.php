<?php
/**
 * @file
 * Contains ContextHandler.php.
 */

namespace Drupal\page_manager;


use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\TypedDataManager;

class ContextHandler {

  /**
   * @var \Drupal\Core\TypedData\TypedDataManager
   */
  protected $typed_data;

  public function __construct(TypedDataManager $typed_data) {
    $this->typed_data = $typed_data;
  }

  public function checkRequirements(array $contexts, array $requirements) {
    $results = array();
    foreach ($requirements as $name => $requirement) {
      if (!$requirement instanceof DataDefinitionInterface) {
        // @todo make a custom exception class.
        throw new \Exception(sprintf('The %s requirement must be an instance of DataDefinitionInterface.', $name));
      }
      if ($requirement->isRequired()) {
        foreach ($contexts as $context) {
          if (!$context instanceof DataDefinitionInterface) {
            // @todo make a custom exception class.
            throw new \Exception('An illegal context was passed, all contexts must be an instance of DataDefinitionInterface.');
          }
          if ($requirement->getDataType() == $context->getDataType()) {
            foreach ($requirement->getConstraints() as $constraint_name => $constraint) {
              if ($context->getConstraint($constraint_name) != $constraint) {
                continue 2;
              }
            }
          }
          $results[$name] = TRUE;
        }
      }
      else {
        $results[$name] = TRUE;
      }
    }
    return count($requirements) == count($results);
  }

  public function getAvailablePlugins(array $contexts, PluginManagerInterface $manager) {
    $plugins = $manager->getDefinitions();
    $available_plugins = array();
    foreach ($plugins as $plugin_id => $plugin) {
      if (isset($plugin['context'])) {
        $plugin_contexts = $plugin['context'];
        $requirements = array();
        foreach ($plugin_contexts as $context_id => $plugin_context) {
          $definition = $this->typed_data->getDefinition($plugin_context['type']);
          if (isset($plugin_context['constraints'])) {
            if (!isset($definition['constraints'])) {
              $definition['constraints'] = $plugin_context['constraints'];
            }
            else {
              $definition['constraints'] += $plugin_context['constraints'];
            }
          }
          if (!isset($definition['required'])) {
            $definition['required'] = TRUE;
          }
          $requirements[$context_id] = new DataDefinition($definition);
        }
        if ($this->checkRequirements($contexts, $requirements)) {
          $available_plugins[$plugin_id] = $plugin;
        }
      }
      else {
        $available_plugins[$plugin_id] = $plugin;
      }
    }
    return $available_plugins;
  }

  public function getValidContexts(array $contexts, DataDefinitionInterface $definition) {
    $valid = array();
    foreach ($contexts as $id => $context) {
      if (!$context instanceof DataDefinitionInterface) {
        // @todo make a custom exception class.
        throw new \Exception('An illegal context was passed, all contexts must be an instance of DataDefinitionInterface.');
      }
      if ($definition->getDataType() == $context->getDataType()) {
        foreach ($definition->getConstraints() as $constraint_name => $constraint) {
          if ($context->getConstraint($constraint_name) != $constraint) {
            continue 2;
          }
        }
      }
      $valid[$id] = $context;
    }
    return $valid;
  }
  
} 