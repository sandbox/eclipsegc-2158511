<?php
/**
 * @file
 * Contains PageHandlerPluginManager.php.
 */

namespace Drupal\page_manager;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Extension\ModuleHandlerInterface;

class PageHandlerPluginManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, LanguageManager $language_manager, ModuleHandlerInterface $module_handler) {
    $this->alterInfo($module_handler, 'page_handler_info');
    $this->setCacheBackend($cache_backend, $language_manager, 'page_handler_plugins');

    parent::__construct('Plugin/PageHandler', $namespaces, 'Drupal\page_manager\Annotation\PageHandler');
  }

} 