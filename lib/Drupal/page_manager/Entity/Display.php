<?php
/**
 * @file
 * Contains \Drupal\page_manager\Entity\Display.
 */

namespace Drupal\page_manager\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageControllerInterface;
use Drupal\page_manager\DisplayInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines a Block configuration entity class.
 *
 * @EntityType(
 *   id = "display",
 *   label = @Translation("Display"),
 *   controllers = {
 *     "storage" = "Drupal\Core\Config\Entity\ConfigStorageController",
 *     "view_builder" = "Drupal\page_manager\DisplayViewBuilder",
 *     "list" = "Drupal\page_manager\DisplayListController",
 *     "form" = {
 *       "default" = "Drupal\page_manager\Form\DisplayFormController",
 *       "delete" = "Drupal\page_manager\Form\DisplaykDeleteForm"
 *     }
 *   },
 *   config_prefix = "display",
 *   fieldable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "edit-form" = "display.admin_edit"
 *   }
 * )
 */
class Display extends ConfigEntityBase implements DisplayInterface {

  /**
   * The label of the display.
   *
   * @var string
   */
  public $label;

  /**
   * The ID of the display.
   *
   * @var string
   */
  public $id;

  /**
   * The display UUID.
   *
   * @var string
   */
  public $uuid;

  /**
   * The route of this display.
   *
   * @var string
   */
  public $route;

  /**
   * @var array
   */
  protected $conditions = array();

  /**
   * The plugin instance settings.
   *
   * @var array
   */
  protected $settings = array();

  /**
   * The page handler plugin id.
   *
   * @var string
   */
  protected $plugin;

  /**
   * The plugin bag that holds the page handler plugin for this display.
   *
   * @var \Drupal\page_manager\PageHandler\PageHandlerPluginBag
   */
  protected $pluginBag;

  /**
   * Overrides \Drupal\Core\Config\Entity\ConfigEntityBase::__construct();
   */
  public function __construct(array $values) {
    parent::__construct($values, 'display');

    //$this->pluginBag = new PageHandlerPluginBag(\Drupal::service('plugin.manager.page_handler'), array($this->plugin), $this->get('settings'), $this->id());
  }

  public function getPluginId() {
    return $this->plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {
    return \Drupal::service('plugin.manager.page_handler')->createInstance($this->plugin);
//    return $this->pluginBag->get($this->plugin);
  }

  /**
   * Overrides \Drupal\Core\Entity\Entity::label();
   */
  public function label($langcode = NULL) {
    return $this->label;
  }

  /**
   * Returns the route of this display.
   */
  public function route() {
    return $this->route;
  }

  public function getConditions() {
    return $this->conditions;
  }

  public function getCondition($row) {
    if (isset($this->conditions[$row])) {
      return $this->conditions[$row];
    }
  }

  public function setCondition($plugin_id, array $configuration, $row = NULL) {
    if (!is_numeric($row)) {
      $row = count($this->conditions);
    }
    $this->conditions[$row] = array(
      'plugin_id' => $plugin_id,
      'configuration' => $configuration,
    );
  }

  public function removeCondition($row) {
    if (isset($this->conditions[$row])) {
      unset($this->conditions[$row]);
      $this->conditions = array_values($this->conditions);
    }
  }

  /**
   * @return bool
   *
   * @todo change this to be relevant instead of just false.
   */
  public function customRoute() {
    return FALSE;
  }

  public function getContexts() {
    $route = \Drupal::service('router.route_provider')->getRouteByName($this->route());
    $parameters = $route->getOption('parameters');
    // Make sure we got a valid array for our parameters.
    $parameters = $parameters ? $parameters : array();
    $contexts = array();
    foreach ($parameters as $id => $parameter) {
      $definition = \Drupal::service('typed_data')->getDefinition($parameter['type']);
      $contexts[$id] = new DataDefinition($definition);
    }
    // @TODO Extract custom contexts and relationship data as contexts.
    return $contexts;
  }

  /**
   * Overrides \Drupal\Core\Config\Entity\ConfigEntityBase::getExportProperties();
   */
  public function getExportProperties() {
    $properties = parent::getExportProperties();
    $names = array(
      'settings',
      'plugin',
    );
    foreach ($names as $name) {
      $properties[$name] = $this->get($name);
    }
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageControllerInterface $storage_controller) {
    parent::preSave($storage_controller);

    $this->set('settings', $this->getPlugin()->getConfiguration());
  }

}
