<?php
/**
 * @file
 * Contains PageManagerWizard.php.
 */

namespace Drupal\page_manager\Route;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\user\TempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PageManagerWizard extends ControllerBase implements FormInterface, ContainerInjectionInterface {

  /**
   * The tempstore factory for use in a wizard.
   *
   * @var \Drupal\user\TempStoreFactory
   */
  protected $tempstore;

  /**
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * @var string
   */
  protected $tempstore_id;

  /**
   * @var string
   */
  protected $display_name;

  /**
   * @var array
   */
  protected $operations = array();

  /**
   * @var string
   */
  protected $step;

  /**
   * @var \Drupal\Core\Form\FormInterface
   */
  protected $form;

  public static function create(ContainerInterface $container) {
    return new static($container->get('user.tempstore'), $container);
  }

  public function __construct(TempStoreFactory $tempstore, ContainerInterface $container) {
    $this->tempstore = $tempstore;
    $this->container = $container;
  }

  public function getFormId() {
    return 'wizard_form';
  }

  /**
   * A wrapper that creates the form wizard for page_manager.
   *
   * @param $display_name
   *   The id of the display we are working against. This may or may not have
   *   been saved yet.
   * @param string $step
   *   The current step of the wizard
   *
   * @return array.
   */
  public function buildForm(array $form, array &$form_state, $tempstore_id = NULL, $display_name = NULL, $step = NULL) {
    $this->tempstore_id = $tempstore_id;
    $this->display_name = $display_name;
    // Get the partial display entity out of the tempstore.
    $display = $this->tempstore->get($tempstore_id)->get($display_name);
    // Get the plugin that will define any configuration operations we need.
    $plugin = $display->getPlugin();
    $definition = $plugin->getPluginDefinition();
    $operations = $this->getOperations(FALSE, $definition);

    // Figure out what step we're on right now.
    if (!empty($operations[$step])) {
      $this->operations = $operations;
      $this->step = $step;
      $this->form = $operations[$step]::create($this->container);
      $form = $this->form->buildForm($form, $form_state, $display, $operations, $step);
      $form['actions'] = $this->actions($display, $operations, $step);
    }
    return $form;
  }

  /**
   * @param boolean $custom_route
   *   Whether we are dealing with a custom route or a system defined route.
   * @param array $definition
   *   The plugin definition for this display.
   * @return array
   *   An array of possible steps for this wizard.
   */
  public function getOperations($custom_route, array $definition = array()) {
    // If we are overriding an existing route, we cannot determine access or
    // menu settings.
    if ($custom_route) {
      $operations = array(
        'start' => '\Drupal\page_manager\Form\PageManagerStartForm',
        'access' => '\Drupal\page_manager\Form\PageManagerAccessForm',
        'menu' => '\Drupal\page_manager\Form\PageManagerMenuForm',
        'criteria' => '\Drupal\page_manager\Form\PageManagerCriteriaForm',
        'context' => '\Drupal\page_manager\Form\PageManagerContextForm',
      );
    }
    else {
      $operations = array(
        'start' => '\Drupal\page_manager\Form\PageManagerStartForm',
        'criteria' => '\Drupal\page_manager\Form\PageManagerCriteriaForm',
        'context' => '\Drupal\page_manager\Form\PageManagerContextForm',
      );
    }
    // Add our plugin's defined operations to our default operations.
    if ($definition && isset($definition['operations'])) {
      $operations += $definition['operations'];
    }
    return $operations;
  }

  protected function actions(EntityInterface $entity, array $operations, $step) {
    $actions = array(
      'submit' => array(
        '#value' => $this->t('Next'),
        '#validate' => array(
          array($this, 'validateForm'),
        ),
        '#submit' => array(
          array($this, 'submitForm'),
        ),
      ),
    );

    $steps = array_keys($operations);
    // Slice to find the operations that occur before the current operation.
    $before = array_slice($operations, 0, array_search($step, $steps));
    // Slice to find the operations that occur after the current operation.
    $after = array_slice($operations, array_search($step, $steps) + 1);

    // If there are not steps after this one, label the button "Finish".
    if (!$after) {
      $actions['submit']['#value'] = t('Finish');
      $actions['submit']['#submit'][] = array($this, 'finish');
    }

    // If there are steps before this one, label the button "previous"
    // otherwise do not display a button.
    if ($before) {
      $actions['previous'] = array(
        '#value' => t('Previous'),
        '#submit' => array(
          array($this, 'previous'),
        ),
        '#limit_validation_errors' => array(),
        '#weight' => -10,
      );
    }

    foreach ($actions as &$action) {
      $action['#type'] = 'submit';
    }
    return $actions;
  }

  public function validateForm(array &$form, array &$form_state) {
    $this->form->validateForm($form, $form_state);
  }
  public function submitForm(array &$form, array &$form_state) {
    $display = $this->tempstore->get($this->tempstore_id)->get($this->display_name);
    $form_state['#entity'] = $display;
    $this->form->submitForm($form, $form_state);
    if ($form_state['values']['op'] == 'Next') {
      // Get the steps by key.
      $steps = array_keys($this->operations);
      // Get the steps after the current step.
      $after = array_slice($this->operations, array_search($this->step, $steps) + 1);
      // Get the steps after the current step by key.
      $after = array_keys($after);

      $form_state['redirect_route']['route_name'] = 'page_manager.wizard';
      $form_state['redirect_route']['route_parameters'] = array(
        'display_name' => $this->display_name,
        'step' => $after[0],
      );
    }
  }

  public function previous(array &$form, array &$form_state) {
    // Get the steps by key.
    $steps = array_keys($this->operations);
    // Get the steps before the current step.
    $before = array_slice($this->operations, 0, array_search($this->step, $steps));
    // Get the steps before the current step by key.
    $before = array_keys($before);
    // Reverse the steps for easy access to the next step.
    $before = array_reverse($before);

    $form_state['redirect_route']['route_name'] = 'page_manager.wizard';
    $form_state['redirect_route']['route_parameters'] = array(
      'display_name' => $this->display_name,
      'step' => $before[0],
    );
  }

}