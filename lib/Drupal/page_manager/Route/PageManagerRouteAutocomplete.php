<?php
/**
 * @file
 * Contains \Drupal\page_manager\Route\PageManagerRouteAutocomplete.
 */

namespace Drupal\page_manager\Route;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PageManagerRouteAutocomplete {

  public function handleAutocomplete(Request $request) {
    $routes = array();
    $result = db_select('menu_router', 'mr')
      ->fields('mr', array('path', 'route_name'))
      ->condition('route_name', '%' . $request->query->get('q') . '%', 'LIKE')
      ->execute();
    foreach ($result as $route) {
      $routes[] = array('value' => $route->route_name, 'label' => $route->path);
    }
    return new JsonResponse($routes);
  }

} 