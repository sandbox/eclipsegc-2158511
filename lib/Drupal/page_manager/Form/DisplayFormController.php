<?php
/**
 * @file
 * Contains DisplayFormController.php.
 */

namespace Drupal\page_manager\Form;

use Drupal\Core\Entity\EntityFormController;
use Drupal\page_manager\PageHandlerPluginManager;
use Drupal\page_manager\Route\PageManagerWizard;
use Drupal\user\TempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DisplayFormController extends EntityFormController {

  /**
   * The PageHandler Plugin Manager.
   *
   * @var \Drupal\page_manager\PageHandlerPluginManager
   */
  protected $manager;

  /**
   * The Tempstore Factory.
   *
   * @var \Drupal\user\TempStoreFactory
   */
  protected $tempstore;

  /**
   * The array of operations for this wizard.
   *
   * @var array
   */
  protected $operations;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $wizard = PageManagerWizard::create($container);
    return new static($container->get('plugin.manager.page_handler'), $container->get('user.tempstore'), $wizard);
  }

  public function __construct(PageHandlerPluginManager $manager, TempStoreFactory $tempstore, PageManagerWizard $wizard) {
    $this->manager = $manager;
    $this->tempstore = $tempstore;
    $this->wizard = $wizard;
    $this->operations = $wizard->getOperations(FALSE);
  }

  public function form(array $form, array &$form_state) {
    $form = parent::form($form, $form_state);
    $start = new $this->operations['start']($this->tempstore, $this->manager);
    $form += $start->buildForm($form, $form_state, $this->entity, $this->operations, $this->tempstore);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array $form, array &$form_state) {
    $entity = parent::submit($form, $form_state);
    $form_state['#entity'] = $entity;
    $start = new $this->operations['start']($this->tempstore, $this->manager);
    $start->submitForm($form, $form_state);


    $steps = array_keys($this->operations);
    $after = array_keys(array_slice($this->operations, array_search('start', $steps) + 1));
    $after = array_shift($after);
    $form_state['redirect_route']['route_name'] = 'page_manager.wizard';
    $form_state['redirect_route']['route_parameters'] = array(
      'display_name' => $entity->id(),
      'step' => $after,
    );
  }

  protected function actions(array $form, array &$form_state) {
    $actions = parent::actions($form, $form_state);
    // Delete should not generally be accessible from a wizard. Set access
    // to false so that others can easily re-enable if they want it.
    if (isset($actions['delete'])) {
      $actions['delete']['#access'] = FALSE;
    }

    $steps = array_keys($this->operations);
    // Slice to find the operations that occur before the current operation.
    $before = array_slice($this->operations, 0, array_search('start', $steps));
    // Slice to find the operations that occur after the current operation.
    $after = array_slice($this->operations, array_search('start', $steps) + 1);

    // If there are steps after this one, label the button "Next" otherwise
    // label it "Finish".
    if ($after) {
      $actions['submit']['#value'] = t('Next');
    }
    else {
      $actions['submit']['#value'] = t('Finish');
      $actions['submit']['#submit'][] = array($this, 'finish');
    }

    // If there are steps before this one, label the button "previous"
    // otherwise do not display a button.
    if ($before) {
      $actions['previous'] = array(
        '#value' => t('Previous'),
        '#submit' => array(
          array($this, 'previous'),
        ),
        '#limit_validation_errors' => array(),
        '#weight' => -10,
      );
    }

    return $actions;
  }

} 