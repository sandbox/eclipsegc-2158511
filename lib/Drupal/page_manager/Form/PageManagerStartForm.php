<?php
/**
 * @file
 * Contains PageManagerStartForm.php.
 */

namespace Drupal\page_manager\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\page_manager\PageHandlerPluginManager;
use Drupal\user\TempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

class PageManagerStartForm implements FormInterface, ContainerInjectionInterface {

  /**
   * @var \Drupal\user\TempStoreFactory
   */
  protected $tempstore;

  /**
   * @var \Drupal\page_manager\PageHandlerPluginManager
   */
  protected $manager;

  /**
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * @var array
   */
  protected $operations;

  public static function create(ContainerInterface $container) {
    return new static($container->get('user.tempstore'), $container->get('plugin.manager.page_handler'));
  }

  public function __construct(TempStoreFactory $tempstore, PageHandlerPluginManager $manager) {
    $this->tempstore = $tempstore;
    $this->manager = $manager;
  }

  public function getFormId() {
    return 'page_manager_wizard_start';
  }

  public function buildForm(array $form, array &$form_state, EntityInterface $entity = NULL, array $operations = NULL) {
    $this->entity = $entity;
    $this->operations = $operations;

    $form['name'] = array(
      '#type' => 'fieldset',
      '#attributes' => array('class' => array('fieldset-no-legend')),
      '#title' => t('Page Information')
    );
    $label = $this->entity->label();
    $form['name']['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Display name'),
      '#required' => TRUE,
      '#size' => 32,
      '#default_value' => !empty($label) ? $label : '',
      '#maxlength' => 255,
      '#disabled' => !empty($label),
    );
    $id = $this->entity->id();
    $form['name']['id'] = array(
      '#type' => 'machine_name',
      '#maxlength' => 128,
      '#machine_name' => array(
        'exists' => 'page_manager_display_load',
        'source' => array('name', 'label'),
      ),
      '#description' => t('A unique machine-readable name for this View. It must only contain lowercase letters, numbers, and underscores.'),
      '#default_value' => $id,
      '#disabled' => !empty($id),
    );
    $route = $this->entity->route();
    $form['route'] = array(
      '#type' => 'textfield',
      '#title' => t('Route to override'),
      '#autocomplete_route_name' => 'page_manager.autocomplete',
      '#default_value' => $route,
    );
    $options = array();
    foreach ($this->manager->getDefinitions() as $plugin_id => $definition) {
      $options[$plugin_id] = $definition['admin_label'];
    }
    $form['plugin'] = array(
      '#type' => 'select',
      '#options' => $options,
      '#title' => t('Display Type'),
      '#default_value' => $this->entity->getPluginId(),
    );
    return $form;
  }

  public function validateForm(array &$form, array &$form_state) {

  }

  public function submitForm(array &$form, array &$form_state) {
    $entity = $form_state['#entity'];
    $entity->route = $form_state['values']['route'];
    $this->tempstore->get('page_manager')->set($entity->id(), $entity);
  }
} 