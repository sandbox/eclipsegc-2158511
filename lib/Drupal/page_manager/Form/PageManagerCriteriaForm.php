<?php
/**
 * @file
 * Contains PageManagerCriteriaForm.php.
 */

namespace Drupal\page_manager\Form;

use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\TypedDataManager;
use Drupal\page_manager\ContextHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\TempStoreFactory;

class PageManagerCriteriaForm implements FormInterface, ContainerInjectionInterface {

  /**
   * @var \Drupal\user\TempStoreFactory
   */
  protected $tempstore;

  /**
   * @var \Drupal\page_manager\ContextHandler
   */
  protected $handler;

  /**
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $manager;

  /**
   * @var \Drupal\Core\TypedData\TypedDataManager
   */
  protected $typed_data;

  /**
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * @var array
   */
  protected $operations;

  public static function create(ContainerInterface $container) {
    return new static($container->get('user.tempstore'), $container->get('context.handler'), $container->get('plugin.manager.condition'), $container->get('typed_data'));
  }

  public function __construct(TempStoreFactory $tempstore, ContextHandler $handler, ConditionManager $manager, TypedDataManager $typed_data) {
    $this->tempstore = $tempstore;
    $this->handler = $handler;
    $this->manager = $manager;
    $this->typed_data = $typed_data;
  }

  public function getFormId() {
    return 'page_manager_wizard_criteria';
  }

  public function buildForm(array $form, array &$form_state, EntityInterface $entity = NULL, array $operations = array(), $step = NULL) {
    $form['entity'] = array(
      '#type' => 'value',
      '#value' => $entity,
    );
    $form['step'] = array(
      '#type' => 'value',
      '#value' => $step,
    );
    $contexts = $entity->getContexts();
    $conditions = $this->manager->getDefinitions();
    $available_conditions = $this->handler->getAvailablePlugins($contexts, $this->manager);
    $options = array();
    foreach ($available_conditions as $condition_id => $condition) {
      $options[$condition_id] = $condition['label'];
    }

    $configured_conditions = array();
    foreach ($entity->getConditions() as $row => $condition) {
      list($plugin_id, $configuration) = array_values($condition);
      $build = array(
        '#type' => 'operations',
        '#links' => $this->getOperations('page_manager.condition', array('entity_id' => $entity->id(), 'step' => $step), $row),
      );
      // @todo compare against available conditions to make sure all
      // conditions are still available.
      $configured_condition = $this->manager->createInstance($plugin_id, $configuration);
      $configured_conditions[] = array(
        $conditions[$plugin_id]['label'],
        $configured_condition->summary(),
        drupal_render($build)
      );
    }
    $form['items'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="configured-conditions">',
      '#suffix' => '</div>',
      '#theme' => 'table',
      '#header' => array('Type', 'Summary', 'Operations'),
      '#rows' => $configured_conditions,
      '#empty' => t('No conditions have been configured.')
    );
    $form['plugin_id'] = array(
      '#type' => 'select',
      '#title' => t('Configure a condition.'),
      '#options' => $options,
    );
    $form['add'] = array(
      '#type' => 'submit',
      '#value' => t('Add condition'),
      '#ajax' => array(
        'accepts' => 'application/vnd.drupal-modal',
        'dialog' => array('width' => '750px'),
        'path' => '/admin/structure/pages/edit/' . $entity->id() . '/' . $step . '/condition/add',
        'wrapper' => 'configured-conditions',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );
    $form['add']['#submit'][] = array($this, 'addCondition');
    $form['and_or'] = array(
      '#type' => 'radios',
      '#title' => t('Succeed if Any or All conditions pass.'),
      '#options' => array(
        'and' => t('All Conditions'),
        'or' => t('Any Condition'),
      ),
      '#default_value' => !empty($entity->and_or) ? $entity->and_or : 'and',
    );

    return $form;
  }

  public function validateForm(array &$form, array &$form_state) {

  }

  public function submitForm(array &$form, array &$form_state) {

  }

  public function addCondition(array &$form, array &$form_state) {
    $entity = $form_state['values']['entity'];
    $step = $form_state['values']['step'];
    $form_state['redirect_route']['route_name'] = 'page_manager.condition.add.nojs';
    $form_state['redirect_route']['route_parameters'] = array(
      'entity_id' => $entity->id(),
      'step' => $step,
      'plugin_id' => $form_state['values']['plugin_id'],
    );
  }

  public function getOperations($route_name_base, array $route_parameters = array(), $row) {
    $route_parameters['row'] = $row;
    $operations['edit'] = array(
      'title' => t('Edit'),
      'route_name' => $route_name_base . '.edit',
      'route_parameters' => $route_parameters,
      'weight' => 10,
      'attributes' => array(
        'class' => array('use-ajax'),
        'data-accepts' => 'application/vnd.drupal-modal',
        'data-dialog-options' => json_encode(array(
          'width' => 750,
        )),
      ),
    );
    $operations['delete'] = array(
      'title' => t('Delete'),
      'route_name' => $route_name_base . '.delete',
      'route_parameters' => $route_parameters,
      'weight' => 100,
      'attributes' => array(
        'class' => array('use-ajax'),
        'data-accepts' => 'application/vnd.drupal-modal',
        'data-dialog-options' => json_encode(array(
          'width' => 750,
        )),
      ),
    );
    return $operations;
  }

}