<?php
/**
 * @file
 * Contains PageManagerCriteriaDeleteForm.php.
 */

namespace Drupal\page_manager\Form;

class PageManagerCriteriaDeleteForm extends ConditionDeleteForm {

  public function submitForm(array &$form, array &$form_state) {
    parent::submitForm($form, $form_state);
    $entity = $form_state['values']['entity'];
    $step = $form_state['values']['step'];
    $form_state['redirect_route']['route_name'] = 'page_manager.wizard';
    $form_state['redirect_route']['route_parameters'] = array(
      'display_name' => $entity->id(),
      'step' => $step,
    );
  }


}