<?php
/**
 * @file
 * Contains ConditionDeleteForm.php.
 */

namespace Drupal\page_manager\Form;

use Drupal\user\TempStoreFactory;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConditionDeleteForm implements FormInterface, ContainerInjectionInterface {

  /**
   * Instantiates a new instance of this controller.
   *
   * This is a factory method that returns a new instance of this object. The
   * factory should pass any needed dependencies into the constructor of this
   * object, but not the container itself. Every call to this method must return
   * a new instance of this object; that is, it may not implement a singleton.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this object should use.
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('user.tempstore'));
  }

  function __construct(TempStoreFactory $tempstore) {
    $this->tempstore = $tempstore;
  }


  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'condition_delete_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param array $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, array &$form_state, $entity_id = NULL, $step = NULL, $tempstore_id = NULL, $row = NULL) {
    $entity = $this->tempstore->get($tempstore_id)->get($entity_id);
    // The nature of tossing this in a modal is sort of weird, and storing
    // these as form values just works better.
    $form['step'] = array(
      '#type' => 'value',
      '#value' => $step,
    );
    $form['tempstore_id'] = array(
      '#type' => 'value',
      '#value' => $tempstore_id,
    );
    $form['row'] = array(
      '#type' => 'value',
      '#value' => $row,
    );
    $form['entity'] = array(
      '#type' => 'value',
      '#value' => $entity,
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Condition'),
    );
    return $form;
  }

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param array $form_state
   *   An associative array containing the current state of the form.
   */
  public function validateForm(array &$form, array &$form_state) {}

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param array $form_state
   *   An associative array containing the current state of the form.
   */
  public function submitForm(array &$form, array &$form_state) {
    $entity = $form_state['values']['entity'];
    $row = $form_state['values']['row'];
    $tempstore_id = $form_state['values']['tempstore_id'];
    $entity->removeCondition($row);
    $this->tempstore->get($tempstore_id)->set($entity->id(), $entity);
  }


} 