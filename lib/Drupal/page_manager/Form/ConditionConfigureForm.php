<?php
/**
 * @file
 * Contains \Drupal\page_manager\Form\ConditionConfigureForm.php.
 */

namespace Drupal\page_manager\Form;


use Drupal\Component\Plugin\ContextAwarePluginInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\page_manager\ContextHandler;
use Drupal\user\TempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Condition\ConditionManager;

class ConditionConfigureForm implements FormInterface, ContainerInjectionInterface {

  /**
   * @var \Drupal\user\TempStoreFactory
   */
  protected $tempstore;

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $manager;

  /**
   * @var \Drupal\page_manager\ContextHandler
   */
  protected $handler;

  /**
   * @var mixed integer|NULL
   */
  protected $row;

  /**
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('user.tempstore'), $container->get('plugin.manager.condition'), $container->get('context.handler'));
  }

  /**
   * Inject the condition plugin manager for internal use.
   */
  public function __construct(TempStoreFactory $tempstore, ConditionManager $manager, ContextHandler $handler) {
    $this->tempstore = $tempstore;
    $this->manager = $manager;
    $this->handler = $handler;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'condition_configure_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state, $plugin_id = NULL, $entity_id = NULL, $step = NULL, $tempstore_id = NULL, $row = NULL) {
    $entity = $this->tempstore->get($tempstore_id)->get($entity_id);
    $configuration = array();
    if (!$plugin_id) {
      // Let's make sure we can even do something.
      $plugin_id = isset($form_state['input']['plugin_id']) ? $form_state['input']['plugin_id'] : '';
      if (empty($plugin_id) && is_numeric($row)) {
        list($plugin_id, $configuration) = array_values($entity->getCondition($row));
      }
      if (!in_array($plugin_id, array_keys($this->manager->getDefinitions()))) {
        throw new \Exception("You must choose a valid condition plugin.");
      }
    }
    // The nature of tossing this in a modal is sort of weird, and storing
    // these as form values just works better.
    $form['step'] = array(
      '#type' => 'value',
      '#value' => $step,
    );
    $form['tempstore_id'] = array(
      '#type' => 'value',
      '#value' => $tempstore_id,
    );
    $form['row'] = array(
      '#type' => 'value',
      '#value' => $row,
    );
    $form['entity'] = array(
      '#type' => 'value',
      '#value' => $entity,
    );
    // We'll store this later. No sense it passing it to itself.
    $condition = $this->manager->createInstance($plugin_id, $configuration);
    // Start Context Selection Generation
    if (empty($configuration)) {
      $configuration = $condition->getConfig();
    }
    if ($condition instanceof ContextAwarePluginInterface) {
      foreach ($condition->getContextDefinitions() as $context_slot => $definition) {
        $definition['required'] = isset($definition['required']) ? $definition['required'] : TRUE;
        $definition = new DataDefinition($definition);
        $valid_contexts = $this->handler->getValidContexts($entity->getContexts(), $definition);
        $options = array();
        foreach ($valid_contexts as $context_id => $context) {
          $options[$context_id] = $context->getLabel();
        }
        $form[$context_slot] = array(
          '#title' => t('Select a @context value:', array('@context' => $context_slot)),
          '#type' => 'select',
          '#options' => $options,
          '#required' => $definition->isRequired(),
          '#default_value' => !empty($configuration['context map'][$context_slot]) ? $configuration['context map'][$context_slot] : '',
        );
      }
    }
    // End Context Selection Generation
    $form['plugin_id'] = array(
      '#type' => 'hidden',
      '#value' => $plugin_id,
    );
    $form = $condition->buildForm($form, $form_state);
    $form['condition_plugin'] = array(
      '#type' => 'value',
      '#value' => $condition,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
    $form['submit']['#validate'][] = array($this, 'validateForm');
    $form['submit']['#submit'][] = array($this, 'submitForm');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
    $form_state['values']['condition_plugin']->validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state){
    $entity = $form_state['values']['entity'];
    $tempstore_id = $form_state['values']['tempstore_id'];
    $row = $form_state['values']['row'];
    $condition = $form_state['values']['condition_plugin'];
    $condition->submitForm($form, $form_state);
    $contexts = array();
    foreach(array_keys($condition->getContextDefinitions()) as $context_slot) {
      if (isset($form_state['values'][$context_slot])) {
        $contexts[$context_slot] = $form_state['values'][$context_slot];
      }
    }
    // @TODO move this to the subclass so that it can be step specific.
    $condition->setConfig('context map', $contexts);
    $entity->setCondition($condition->getPluginId(), $condition->getConfig(), $row);

    $this->tempstore->get($tempstore_id)->set($entity->id(), $entity);
  }

} 