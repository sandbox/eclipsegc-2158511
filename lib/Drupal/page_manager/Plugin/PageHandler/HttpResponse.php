<?php
/**
 * @file
 * Contains HttpResponse.php.
 */

namespace Drupal\page_manager\Plugin\PageHandler;

use Drupal\Core\Plugin\PluginBase;
use Drupal\page_manager\PageHandlerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @PageHandler(
 *   id = "http_response",
 *   admin_label = @Translation("HTTP Response Code"),
 *   operations = {
 *     "settings" = "Drupal\page_manager\Form\HttpResponseSettingsForm"
 *   }
 * )
 */
class HttpResponse extends PluginBase implements PageHandlerInterface {

  public function handle(Request $request) {

  }
} 