<?php
/**
 * @file
 * Contains \Drupal\page_manager\Annotation\PageHandler.
 */

namespace Drupal\page_manager\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a PageHanlder annotation object.
 *
 * @Annotation
 */
class PageHandler extends Plugin {

  public $id;

  public $admin_label;

  public $operations = array();

} 